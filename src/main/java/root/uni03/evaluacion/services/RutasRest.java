/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.uni03.evaluacion.services;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.uni03.evaluacion.dao.RutasTrekkingJpaController;
import root.uni03.evaluacion.dao.exceptions.NonexistentEntityException;
import root.uni03.evaluacion.entity.RutasTrekking;

/**
 *
 * @author evelyn
 */

@Path("rutas")
public class RutasRest {
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarRutas(){
    
        RutasTrekkingJpaController dao = new RutasTrekkingJpaController();
        
        List<RutasTrekking> rutas = dao.findRutasTrekkingEntities();
       
        return Response.ok(200).entity(rutas).build();
    }
    
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(RutasTrekking ruta) {
    
        RutasTrekkingJpaController dao = new RutasTrekkingJpaController();
        
        try {
            dao.create(ruta);
        } catch (Exception ex) {
            Logger.getLogger(RutasRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok(200).entity(ruta).build();
        
        
    }
    
    
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizar(RutasTrekking ruta) {
    
        RutasTrekkingJpaController dao = new RutasTrekkingJpaController();
        try {
            dao.edit(ruta);
        } catch (Exception ex) {
            Logger.getLogger(RutasRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(200).entity(ruta).build();
    }
    
    
    @DELETE
    @Path("/{ideliminar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response eliminar(@PathParam("ideliminar") String ideliminar){
    
        RutasTrekkingJpaController dao = new RutasTrekkingJpaController();
        try {
            dao.destroy(ideliminar);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(RutasRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok("ruta eliminada").build();
    }
    
    
    @GET
    @Path("/{idconsulta}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response consultarporId(@PathParam("idconsulta") String idconsulta){
    
        RutasTrekkingJpaController dao = new RutasTrekkingJpaController();
        RutasTrekking ruta = dao.findRutasTrekking(idconsulta);
        
        return Response.ok(200).entity(ruta).build();
    }
    
}

















