/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.uni03.evaluacion.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.uni03.evaluacion.dao.exceptions.NonexistentEntityException;
import root.uni03.evaluacion.dao.exceptions.PreexistingEntityException;
import root.uni03.evaluacion.entity.RutasTrekking;

/**
 *
 * @author evely
 */
public class RutasTrekkingJpaController implements Serializable {

    public RutasTrekkingJpaController() {
        
    }
    
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("rutas_PU");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(RutasTrekking rutasTrekking) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(rutasTrekking);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findRutasTrekking(rutasTrekking.getRuta()) != null) {
                throw new PreexistingEntityException("RutasTrekking " + rutasTrekking + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(RutasTrekking rutasTrekking) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            rutasTrekking = em.merge(rutasTrekking);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = rutasTrekking.getRuta();
                if (findRutasTrekking(id) == null) {
                    throw new NonexistentEntityException("The rutasTrekking with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            RutasTrekking rutasTrekking;
            try {
                rutasTrekking = em.getReference(RutasTrekking.class, id);
                rutasTrekking.getRuta();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The rutasTrekking with id " + id + " no longer exists.", enfe);
            }
            em.remove(rutasTrekking);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<RutasTrekking> findRutasTrekkingEntities() {
        return findRutasTrekkingEntities(true, -1, -1);
    }

    public List<RutasTrekking> findRutasTrekkingEntities(int maxResults, int firstResult) {
        return findRutasTrekkingEntities(false, maxResults, firstResult);
    }

    private List<RutasTrekking> findRutasTrekkingEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(RutasTrekking.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public RutasTrekking findRutasTrekking(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(RutasTrekking.class, id);
        } finally {
            em.close();
        }
    }

    public int getRutasTrekkingCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<RutasTrekking> rt = cq.from(RutasTrekking.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}


