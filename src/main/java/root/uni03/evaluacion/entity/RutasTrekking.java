/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.uni03.evaluacion.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author evelyn
 */
@Entity
@Table(name = "rutas_trekking")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RutasTrekking.findAll", query = "SELECT r FROM RutasTrekking r"),
    @NamedQuery(name = "RutasTrekking.findByRuta", query = "SELECT r FROM RutasTrekking r WHERE r.ruta = :ruta"),
    @NamedQuery(name = "RutasTrekking.findByUbicacion", query = "SELECT r FROM RutasTrekking r WHERE r.ubicacion = :ubicacion"),
    @NamedQuery(name = "RutasTrekking.findByDistancia", query = "SELECT r FROM RutasTrekking r WHERE r.distancia = :distancia"),
    @NamedQuery(name = "RutasTrekking.findByDificultad", query = "SELECT r FROM RutasTrekking r WHERE r.dificultad = :dificultad"),
    @NamedQuery(name = "RutasTrekking.findBySe\u00f1alizacion", query = "SELECT r FROM RutasTrekking r WHERE r.se\u00f1alizacion = :se\u00f1alizacion")})
public class RutasTrekking implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ruta")
    private String ruta;
    @Size(max = 100)
    @Column(name = "ubicacion")
    private String ubicacion;
    @Size(max = 5)
    @Column(name = "distancia")
    private String distancia;
    @Size(max = 50)
    @Column(name = "dificultad")
    private String dificultad;
    @Size(max = 100)
    @Column(name = "se\u00f1alizacion")
    private String señalizacion;

    public RutasTrekking() {
    }

    public RutasTrekking(String ruta) {
        this.ruta = ruta;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getDistancia() {
        return distancia;
    }

    public void setDistancia(String distancia) {
        this.distancia = distancia;
    }

    public String getDificultad() {
        return dificultad;
    }

    public void setDificultad(String dificultad) {
        this.dificultad = dificultad;
    }

    public String getSeñalizacion() {
        return señalizacion;
    }

    public void setSeñalizacion(String señalizacion) {
        this.señalizacion = señalizacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ruta != null ? ruta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RutasTrekking)) {
            return false;
        }
        RutasTrekking other = (RutasTrekking) object;
        if ((this.ruta == null && other.ruta != null) || (this.ruta != null && !this.ruta.equals(other.ruta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.uni03.evaluacion.entity.RutasTrekking[ ruta=" + ruta + " ]";
    }
    
}



