<%-- 
    Document   : index
    Created on : 01-07-2021, 20:33:35
    Author     : evelyn
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Endpoints Api Rutas de Trekking</title>
    </head>
    <body>
        <h1>Endpoints Api Rutas de Trekking</h1>
        <h2>Lista de rutas de trekking - GET - https://uni03-evaluacion.herokuapp.com/api/rutas</h2>
        <h2>Crear rutas - POST - https://uni03-evaluacion.herokuapp.com/api/rutas</h2>
        <h2>Actualizar rutas - PUT - https://uni03-evaluacion.herokuapp.com/api/rutas</h2>
        <h2>Eliminar rutas - DELETE - https://uni03-evaluacion.herokuapp.com/api/rutas/{id}</h2>
        <h2>Lista rutas por id - GET - https://uni03-evaluacion.herokuapp.com/api/rutas/{id}</h2>
    </body>
</html>
